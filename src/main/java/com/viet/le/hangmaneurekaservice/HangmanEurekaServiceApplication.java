package com.viet.le.hangmaneurekaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class HangmanEurekaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HangmanEurekaServiceApplication.class, args);
	}
}


